<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include 'metadados.php'; ?>
    <link href="css/index.css" rel="stylesheet">
    <link rel="icon" href="img/logo.png">
	<title>Index</title>
</head>
<body>
	<div class="row index-rodape">
		<div class="container">
			<footer>
				<p class="text-center ">&copy; Formulários online</p>
			</footer>
		</div>
	</div>
</body>
</html>