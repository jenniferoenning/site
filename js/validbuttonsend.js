$("#send").on('click', function (valid) {
  valid.preventDefault();

  if ($('#cpf')[0].value == '') {
    swalFire({
      title: 'Cpf inválido!',
      text: '',
      type: 'error',
      confirmButtonText: 'Ok'
    });
  }else if ($('#nome')[0].value == '') {
    swalFire({
      title: 'Nome inválido!',
      text: '',
      type: 'error',
      confirmButtonText: 'Ok'
    });
  }else if ($('#tel')[0].value == '') {
    swalFire({
      title: 'Telefone inválido!',
      text: '',
      type: 'error',
      confirmButtonText: 'Ok'
    });
  }else {
    swalFire({
      position: 'top-end',
      type: 'success',
      title: 'Seu formulário foi enviado!',
      showConfirmButton: false,
      timer: 1500
    });
  }
});

function swalFire(param) {
  Swal.fire(param)
}