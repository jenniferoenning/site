<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include 'metadados.php'; ?>
    <link href="css/about.css" rel="stylesheet">
    <link rel="icon" href="img/logo.png">
	<title>Moda News</title>
</head>
<body>
<body>
    <div class="banner text-sm ">
        <div class="container-fluid px-lg-5 py-3">
            <div class="row align-items-center">
                <div class="col-sm-6 col-md-4 text-left text-md-center">
                    <a class="btn btn-gray btn-sm font-button" href="http://aula.local/form.php?">Contato</a>
                    <a class="btn btn-gray btn-sm font-button" href="http://aula.local/about.php?">Sobre</a>
                </div>
                <div class="font-banner col-sm-6 col-md-4 text-left text-md-center">
                    <a href="http://aula.local/blog.php?"><img class="img-banner" src="img/banner.png">ModaNews</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>