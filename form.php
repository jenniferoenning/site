<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include 'metadados.php'; ?>
    <link href="css/form.css" rel="stylesheet">
    <link rel="icon" href="img/logo.png">
    <title>Formulário</title>
</head>
<body>
    <div class="banner text-sm">
        <div class="container-fluid px-lg-5 py-3">
            <div class="row align-items-center">
                <div class="col-sm-6 col-md-4 text-left text-md-center">
                    <a class="btn btn-gray btn-sm font-button" href="http://aula.local/blog.php?">Blog</a>
                    <a class="btn btn-gray btn-sm font-button" href="http://aula.local/about.php?">Sobre</a>
                </div>
                <div class="font-banner col-sm-6 col-md-4 text-left text-md-center">
                    <a class="" href="http://aula.local/blog.php?"><img class="img-banner" src="img/banner.png">Formulário</a>
                </div>
            </div>
        </div>
    </div>
    <form>
        <div class="text-center">
            <input class="nome" id="nome" type="text" placeholder="Digite seu nome" />
            <p></p>
            <input type="radio" class="button-masculino" id="masculino" name="sex" value="male" required=""> Masculino
            <p></p>
            <input type="radio" class="button-feminino" id="feminino" name="sex" value="female" required=""> Feminino
            <p></p>
            <input class="cpf" id="cpf" type="text" maxlength="14" placeholder="Digite seu CPF" />
            <p></p>
            <input class="tel" id="tel" type="text" maxlength="14" placeholder="Digite seu telefone" />
            <p></p>
        </div>
        <div class="text-center">
            <button class="send-button1" id="send">Enviar</button>
            <button class="limpar-button" type="reset">Limpar</button>
            <p></p>
        </div>
    </form>
        <hr>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.js"integrity="sha256-BTlTdQO9/fascB1drekrDVkaKd9PkwBymMlHOiG+qLI=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="js/jquery.mask.min.js"></script>
        <script src="js/masks.js"></script>
        <div class="row form-rodape">
            <div class="container">
                <footer>
                    <p class="text-center ">&copy; Formulários online</p>
                </footer>
            </div>
        </div>
    <?php include 'scripts.php';  ?>
</body>
</html>