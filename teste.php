<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include 'metadados.php'; ?>
    <link href="css/teste.css" rel="stylesheet">
    <link rel="icon" href="img/logo.png">
    <title>Formulário</title>
</head>
<body>
    <section>
            <div class="section-bar-container">
                <div class="wrapper-center">
                    <div class="box-phone hidden-xs hidden-sm">
                        <p class="phone-contact">Participe via WhatsApp (46)99940-3300 ou Email:maciel@ppnewsfb.com.br</p>
                    </div>
                </div>
            </div>
    </section>
    <div class="nav-bar text-sm">
        <div class="container-fluid px-lg-5 py-3">
            <div class="row align-items-center">
                <div class="font-banner col-sm-6 col-md-4 text-left text-md-center">
                    <img class="img-banner" src="img/portalppnewsmaciel.jpg"><a href="/"></a>
                </div>
            </div>
        </div>
    </div>

</body>
</html>